package com.example.demo.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Pay;
import com.example.demo.repository.PayRepository;


/**
 * @author Sanyuja Kharat
 *
 */
@Service
public class PayService {
	
	@Autowired
	private PayRepository payRepository;
	
	public Pay save(Pay pay, double total) {
		
		String orderId = UUID.randomUUID().toString();
		String customerId = UUID.randomUUID().toString();
		pay.setOrderid(orderId);
		pay.setCustomerid(customerId);
		pay.setChannel("Web");
		pay.setTotal(total);
		return payRepository.save(pay);
		
	}

}
