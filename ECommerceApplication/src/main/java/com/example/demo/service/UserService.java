package com.example.demo.service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.demo.exception.CustomException;
import com.example.demo.model.Role;
import com.example.demo.model.User;
import com.example.demo.model.VerificationToken;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.repository.VerificationTokenRepository;


/**
 * @author Sanyuja Kharat
 *
 */
@Service
public class UserService {

	private static final int EXPIRATION = 60 * 24;
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private VerificationTokenRepository verificationRepository;

	public User saveUser(User user) throws CustomException {
		
		User user1 = userRepository.findByEmail(user.getEmail());
		if(user1 !=null) {
			throw new CustomException("There is an account with emailid");
		}

		 Role role = roleRepository.findByName("CUSTOMER");
		user.setEnabled(true);
		user.setRoles(Arrays.asList(role));
		encodePassword(user);
		return userRepository.save(user);

	}
	
	public void encodePassword(User user) {
		String encodedpassword = passwordEncoder.encode(user.getPassword());
		user.setPassword(encodedpassword);
		
		
	}
	
	public void getVerificationToken(String token) throws CustomException {
		Optional<VerificationToken> tokenOptional = Optional.ofNullable(verificationRepository.findByToken(token));
		if (!tokenOptional.isPresent()) {
			throw new CustomException("Token Expired");
		}

		LocalDateTime tokenCreationDate = tokenOptional.get().getToken_creation_date();

		if (isTokenExpired(tokenCreationDate)) {
			throw new CustomException("Token Expired");
		}

		User user = tokenOptional.get().getUser();
		user.setEnabled(true);

		userRepository.save(user);

	}

	private boolean isTokenExpired(final LocalDateTime tokenCreationdate) {

		LocalDateTime now = LocalDateTime.now();
		Duration diff = Duration.between(tokenCreationdate, now);
		return diff.toMinutes() >= EXPIRATION;

	}


	
	public void createVerificationToken(User user, String token) {
		VerificationToken verificationToken = new VerificationToken();
		verificationToken.setToken(token);
		verificationToken.setUser(user);
//		verificationToken.setExpiredDateTime(expiredDateTime);
		verificationToken.setToken_creation_date(LocalDateTime.now());
		verificationRepository.save(verificationToken);

	}

	
	
	

}
