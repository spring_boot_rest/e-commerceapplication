package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Product;
import com.example.demo.repository.ProductRepository;

/**
 * @author Sanyuja Kharat
 *
 */
@Service
public class ProductService {

	@Autowired
	private ProductRepository productRepository;

	public List<Product> getAllProducts() {
		return (List<Product>) productRepository.findAll();

	}

	public void addProduct(Product product) {
		productRepository.save(product);

	}

	public void removeProductById(long id) {
		productRepository.deleteById(id);

	}

	public Optional<Product> getProductByid(long id) {
		return productRepository.findById(id);

	}

	public List<Product> getAllProductsByCategory(int id) {
		return productRepository.findAllByCategory_Id(id);

	}

}
