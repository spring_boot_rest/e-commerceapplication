package com.example.demo.controller;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.model.Pay;
import com.example.demo.model.Paytmdetails;
import com.example.demo.service.PayService;
import com.paytm.pg.merchant.PaytmChecksum;
/**
 * @author Sanyuja Kharat 
 *
 */
@Controller
public class CheckOut1Controller {
	
	@Autowired
	private Paytmdetails paytmDetails;
	
	@Autowired
	private PayService payService;
	
	@Autowired
	private Environment env;

	@PostMapping("/payNow/{total}")
//	public String payNow(@ModelAttribute("total") Double total, Model model) {
	public String payNow(@PathVariable("total") Double total, Model model) {
		Pay pay=new Pay();
		
		Pay payUser = payService.save(pay, total);
		model.addAttribute("payUser", payUser);
		
		return "payNow";
	}
	
	@PostMapping("/pgredirect")
	public ModelAndView getRedirect(@ModelAttribute("payUser") Pay payUser, Model model) throws Exception {
		ModelAndView modelAndView = new ModelAndView("redirect:" + paytmDetails.getPaytmUrl());
		
		//String view=paytmDetails.getPaytmUrl();
        TreeMap<String, String> parameters = new TreeMap<>();
        paytmDetails.getDetails().forEach((k, v) -> parameters.put(k, v));
        parameters.put("MOBILE_NO", env.getProperty("paytm.mobile"));
        parameters.put("EMAIL", env.getProperty("paytm.email"));
        parameters.put("ORDER_ID", payUser.getOrderid());
        parameters.put("TXN_AMOUNT", String.valueOf(payUser.getTotal()));
        parameters.put("CUST_ID", payUser.getCustomerid());
        String checkSum = getCheckSum(parameters);
        parameters.put("CHECKSUMHASH", checkSum);
        modelAndView.addAllObjects(parameters);
//        model.addAllAttributes(parameters);
        return modelAndView;

		
	}
	
	@PostMapping(value = "/pgresponse")
    public String getResponseRedirect(HttpServletRequest request, Model model) {

        Map<String, String[]> mapData = request.getParameterMap();
        TreeMap<String, String> parameters = new TreeMap<String, String>();
        String paytmChecksum = "";
        for (Entry<String, String[]> requestParamsEntry : mapData.entrySet()) {
            if ("CHECKSUMHASH".equalsIgnoreCase(requestParamsEntry.getKey())){
                paytmChecksum = requestParamsEntry.getValue()[0];
            } else {
            	parameters.put(requestParamsEntry.getKey(), requestParamsEntry.getValue()[0]);
            }
        }
        String result;

        boolean isValideChecksum = false;
        System.out.println("RESULT : "+parameters.toString());
        try {
            isValideChecksum = validateCheckSum(parameters, paytmChecksum);
            if (isValideChecksum && parameters.containsKey("RESPCODE")) {
                if (parameters.get("RESPCODE").equals("01")) {
                    result = "Payment Successful";
                } else {
                    result = "Payment Failed";
                }
            } else {
                result = "Checksum mismatched";
            }
        } catch (Exception e) {
            result = e.toString();
        }
        model.addAttribute("result",result);
        parameters.remove("CHECKSUMHASH");
        model.addAttribute("parameters",parameters);
        return "report";
    }

    private boolean validateCheckSum(TreeMap<String, String> parameters, String paytmChecksum) throws Exception {
        return PaytmChecksum.verifySignature(parameters,
        		paytmDetails.getMerchantKey(), paytmChecksum);
    }


private String getCheckSum(TreeMap<String, String> parameters) throws Exception {
	return PaytmChecksum.generateSignature(parameters, paytmDetails.getMerchantKey());
}





}
