package com.example.demo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.model.Product;
import com.example.demo.model.ProductDto;
import com.example.demo.service.CategoryService;
import com.example.demo.service.ProductService;
/**
 * @author Sanyuja Kharat 
 *
 */
@Controller
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private CategoryService categoryService;
	
	@GetMapping("/allproducts")
	public String getProducts(Model model) {
		model.addAttribute("products", productService.getAllProducts());
		return "products";
		
	}
	
	@GetMapping("/product/add")
	public String productGet(Model model) {
		model.addAttribute("productDTO", new ProductDto());
		model.addAttribute("categories", categoryService.getAllcategories());
		return "productsAdd";
		
	}	
	
	  @PostMapping("/product/add")
	  public String productAddPost(@ModelAttribute("productDTO") ProductDto productDTO, @RequestParam("productImage") MultipartFile file) {
		  Product product=new Product();
		  product.setId(productDTO.getId());
		  product.setName(productDTO.getName());
		  product.setCategory(categoryService.getCategoryById(productDTO.getCategoryId()).get());
		  product.setPrice(productDTO.getPrice());
		  product.setDescription(productDTO.getDescription());
		  
		  productService.addProduct(product);
		  return "redirect:/allproducts";
	  
	  }
	  
	  @GetMapping("/product/delete/{id}")
	  public String deleteProduct(@PathVariable long id) {
		  
		  productService.removeProductById(id);
		  
		  return "redirect:/allproducts";
		  
		  
	  }
	  
	  @GetMapping("/product/update/{id}")
	  public String deleteProduct(@PathVariable long id,Model model) {
		  
		   Product product = productService.getProductByid(id).get();
		   ProductDto productDTO=new ProductDto();
		   productDTO.setId(product.getId());
		   productDTO.setName(product.getName());
		   productDTO.setCategoryId(product.getCategory().getId());
		   productDTO.setPrice(product.getPrice());
		   productDTO.setDescription(product.getDescription());
		    
		   model.addAttribute("categories", categoryService.getAllcategories());
		   model.addAttribute("productDTO", productDTO);
			  
		  
		  return "productsAdd";
		  
		  
	  }
	 

}
