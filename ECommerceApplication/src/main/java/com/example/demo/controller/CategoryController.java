package com.example.demo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.example.demo.model.Category;
import com.example.demo.service.CategoryService;

/**
 * @author Sanyuja Kharat 
 *
 */
@Controller
public class CategoryController {
	
	@Autowired
	private CategoryService categoryService;
	
	@GetMapping("/category")
	public String getCategory(Model model) {
		model.addAttribute("categories", categoryService.getAllcategories());
		return "categories";
	}
	
	@GetMapping("/category/add")
	public String getCategories(Model model) {
		model.addAttribute("category", new Category());
		return "categoryAdd";
	}
	
	@PostMapping("/category/add")
	public String postCategories(@ModelAttribute("category") Category category) {
		categoryService.addCategory(category);
		return "redirect:/category";
	}
	
	@GetMapping("/category/delete/{id}")
	public String deleteCat(@PathVariable int id) {
		categoryService.removeCategoryById(id);
		return "redirect:/category";
		
	}
	
	@GetMapping("/category/update/{id}")
	public String updateCategory(@PathVariable int id, Model model) {
		Optional<Category> category = categoryService.getCategoryById(id);
		if(category.isPresent()) {
			model.addAttribute("category", category.get());
			return "categoryAdd";
			
		}else {
			return "404";
		}
		
	}

}
