package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.event.OnRegistrationCompleteEvent;
import com.example.demo.exception.CustomException;
import com.example.demo.model.User;
import com.example.demo.service.UserService;

/**
 * @author Sanyuja Kharat 
 *
 */
@Controller
public class CustomerController {
	
	@Autowired
	private UserService userService;
	
//	@Autowired
//	private ApplicationEventPublisher eventPublisher;
	
	@GetMapping("/signin")
	public String signIn(Model model) {
		
		User user=new User();
		model.addAttribute("user", user);
		
		return "SignIn";
		
	}
	
	@PostMapping("/users/save")
	public String saveUser(User user, RedirectAttributes redirectAttributes) throws CustomException {
		User user1 = userService.saveUser(user);
		redirectAttributes.addFlashAttribute("message", "Information is saved");
//		try {
//			eventPublisher.publishEvent(new OnRegistrationCompleteEvent(user1));
//		} catch (RuntimeException ex) {
//		}
	
		return "redirect:/login";
		
	}
	
	

}
