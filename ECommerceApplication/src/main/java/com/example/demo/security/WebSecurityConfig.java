package com.example.demo.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import com.example.demo.handler.CustomSuccessHandler;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Bean
	public UserDetailsService userDetailsService() {
		return new ShopUserDetailsService();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
    public LogoutSuccessHandler logoutSuccessHandler() {
        return new CustomSuccessHandler();
    }

	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userDetailsService());
		authProvider.setPasswordEncoder(passwordEncoder());
		return authProvider;
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authenticationProvider());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// TODO Auto-generated method stub
		/*
		 * http.authorizeRequests().anyRequest().permitAll();
		 * 
		 * http.authorizeRequests().antMatchers("/","ShopApp","ShopApp/shop/","/shop
		 *//** "," *//*
						 * register").permitAll()
						 * .antMatchers("/categories","/categories/**","/products","/products/**").
						 * hasRole("ADMIN").anyRequest()
						 * .authenticated().and().formLogin().loginPage("/login").permitAll().
						 * failureUrl("/login?error= true")
						 * .defaultSuccessUrl("/").usernameParameter("email").passwordParameter(
						 * "password").and().logout() .logoutRequestMatcher(new
						 * AntPathRequestMatcher("/logout")).logoutSuccessUrl("/login")
						 * .invalidateHttpSession(true).deleteCookies("JSESSIONID").and().
						 * exceptionHandling().and().csrf() .disable();
						 */

		http.authorizeRequests()
		.antMatchers("/", "ShopApp", "ShopApp/shop/", "/shop/** ", "/").permitAll()
				.antMatchers("/categories", "/categories/**", "/products", "/products/**").hasRole("ADMIN")
				.and()
				.formLogin().
						loginPage("/login")
						.usernameParameter("email")
						.passwordParameter("password")
						.defaultSuccessUrl("/").permitAll()
				.and()
				.logout()
//					.logoutUrl("/logout")
					.logoutSuccessUrl("/").permitAll()
				.invalidateHttpSession(true).deleteCookies("JSESSIONID");//.logoutSuccessHandler(logoutSuccessHandler()); //.and().csrf();

			http.csrf().disable();
		
//	      .logout().logoutUrl("/logout").logoutSuccessUrl("/");

	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		// TODO Auto-generated method stub
		web.ignoring().antMatchers("/resources/**", "/static/**", "*/images/**", "/productImages/**", "*/css/**",
				"/js/**");
	}

	/*
	 * @Override protected void configure(AuthenticationManagerBuilder auth) throws
	 * Exception { auth.authenticationProvider(authenticationProvider()); }
	 */

}
