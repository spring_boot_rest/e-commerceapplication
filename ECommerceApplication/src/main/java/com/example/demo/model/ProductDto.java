package com.example.demo.model;


/**
 * @author Sanyuja Kharat
 *
 */
public class ProductDto {

	private Long id;

	private String name;

	private int categoryId;

	private double price;

	private String description;

	public ProductDto() {

	}

	public ProductDto(Long id, String name, int categoryId, double price, String description) {
		super();
		this.id = id;
		this.name = name;
		this.categoryId = categoryId;
		this.price = price;
		this.description = description;
	}
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "ProductDto [id=" + id + ", name=" + name + ", categoryId=" + categoryId + ", price=" + price
				+ ", description=" + description + "]";
	}

}
