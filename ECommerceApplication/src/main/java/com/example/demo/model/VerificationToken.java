package com.example.demo.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * @author Sanyuja Kharat
 *
 */
@Entity
@Table(name = "verificationToken")
public class VerificationToken {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@Column(name = "token")
	private String token;
	@Column(name = "status")
	private String status;
	@Column(name = "token_creation_date", columnDefinition = "TIMESTAMP")
	private LocalDateTime token_creation_date;
	@Column(name = "expiredDateTime", columnDefinition = "TIMESTAMP")
	private LocalDateTime expiredDateTime;
	@OneToOne
	@JoinColumn(name = "user_id")
	private User user;

	public VerificationToken() {

	}

	public VerificationToken(int id, String token, String status, LocalDateTime token_creation_date,
			LocalDateTime expiredDateTime, User user) {
		this.id = id;
		this.token = token;
		this.status = status;
		this.token_creation_date = token_creation_date;
		this.expiredDateTime = expiredDateTime;
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public LocalDateTime getToken_creation_date() {
		return token_creation_date;
	}

	public void setToken_creation_date(LocalDateTime token_creation_date) {
		this.token_creation_date = token_creation_date;
	}

	public LocalDateTime getExpiredDateTime() {
		return expiredDateTime;
	}

	public void setExpiredDateTime(LocalDateTime expiredDateTime) {
		this.expiredDateTime = expiredDateTime;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "VerificationToken [id=" + id + ", token=" + token + ", status=" + status + ", token_creation_date="
				+ token_creation_date + ", expiredDateTime=" + expiredDateTime + ", user=" + user + "]";
	}

}
