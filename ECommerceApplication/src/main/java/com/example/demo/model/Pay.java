package com.example.demo.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @author Sanyuja Kharat
 *
 */
@Entity
@Table(name = "pay")
public class Pay {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String orderid;

	private String customerid;

	private String channel;

	private double total;

	public Pay() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOrderid() {
		return orderid;
	}

	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}

	public String getCustomerid() {
		return customerid;
	}

	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return "Pay [id=" + id + ", orderid=" + orderid + ", customerid=" + customerid + ", channel=" + channel
				+ ", total=" + total + "]";
	}

}
