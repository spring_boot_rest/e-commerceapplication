package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Pay;

/**
 * @author Sanyuja Kharat
 *
 */
@Repository
public interface PayRepository extends CrudRepository<Pay, Integer> {

}
