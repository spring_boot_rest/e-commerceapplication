package com.example.demo.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Role;

/**
 * @author Sanyuja Kharat
 *
 */
@Repository
public interface RoleRepository extends CrudRepository<Role, Integer> {

	@Query(value = "select * from roles where name=:name", nativeQuery = true)
	Role findByName(String name);

}
