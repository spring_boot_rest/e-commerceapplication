package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.VerificationToken;

/**
 * @author Sanyuja Kharat
 *
 */
@Repository
public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Integer> {
	VerificationToken findByUserEmail(String token);

	VerificationToken findByToken(String token);
	// void createVerificationToken(User user,String token);

}
