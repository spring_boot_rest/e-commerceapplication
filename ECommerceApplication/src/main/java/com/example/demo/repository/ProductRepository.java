package com.example.demo.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Product;

/**
 * @author Sanyuja Kharat
 *
 */
@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {
	List<Product> findAllByCategory_Id(int id);

}
