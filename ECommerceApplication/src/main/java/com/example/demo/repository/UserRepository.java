package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.User;

/**
 * @author Sanyuja Kharat
 *
 */
@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

//	@Query(value="select * from user u where u.email = :email",nativeQuery = true)
//	public User getUserByEmail(@Param("email") String email);

	User findByEmail(String email);

}
